# Prepare Virtual Machine with OSGeoLive snapshot for the SIL workshop

In order to execute the exercises during the SIL 'Spatial freshwater biodiversity
analyses' workshop you will need to install the OSGeoLive Linux Virtual Machine.

[OSGeoLive](https://live.osgeo.org/en/index.html) is a self-contained bootable DVD,
USB thumb drive or Virtual Machine based on Lubuntu, that allows you to try a wide
variety of open source geospatial software without installing anything. It is
composed entirely of free software, allowing it to be freely distributed, duplicated
and passed around (source <https://live.osgeo.org/en/index.html>).

On the OSGeoLive snapshot we provide for this workshop all GIS software, R and Rstudio
as well as all R packages are already installed.

For running a Virtual Machine in your OS we need a virtualization software such
as [Virtualbox](https://www.virtualbox.org/wiki/Downloads) and a ova file that
contains the virtualized OS.

## Install Virtualbox

Open you browser and go to <https://www.virtualbox.org/wiki/Downloads> and
based on your OS download the Virtualbox executable and install it.

## Download OSGeoLive snapshot

Download the OSGeoLive snapshot[here](https://nimbus.igb-berlin.de/index.php/s/ctFm4ezsCRWRqG5).

## Install the OSGeoLive snapshot inside VirtualBox

Lunch VirtualBox from OS and follow the below instructions.

**VirtualBox**

Start the VirtualBox application and click on "Import".

![VM Step1](/Setup_virtual_machine/Screenshots/vm_step1.png)

Select the downloaded osgeolive-14.ova file and press "Next".

![VM Step2](/Setup_virtual_machine/Screenshots/vm_step2.png)

Keep all settings and click on "Import".

![VM Step3](/Setup_virtual_machine/Screenshots/vm_step3.png)

<!--
**Sharing folder**

Create an empty folder named VM_shared in your OS.

MAC OS X: /Users/yourname/VM_shared

Windows OS: C:\Users\yourname\Documents\VM_shared

Linux:/home/yourname/VM_shared

![VM Step4](/Setup_virtual_machine/Screenshots/vm_step4.png)

Add a new shared folder in the VirtualBox.
Navigate to the host OS VM_shared folder.
Activate the Auto-mount option and click "OK".

![VM Step5](/Setup_virtual_machine/Screenshots/vm_step5.png)
-->

**Virtual machine settings**

In VirtualBox -> Settings -> General

Set Shared Clipboard and Drag and Drop to Bidirectional.

Do not change the Snapshot Folder.

In VirtualBox -> Settings -> General -> System -> Motherboard, allocate 6GB of memory if possible.

![VM Step6](/Setup_virtual_machine/Screenshots/vm_step6.png)

**Snapshot**

Create a snapshot of the current version. It will be useful for going back if
something is not working.

![VM Step7](/Setup_virtual_machine/Screenshots/vm_step7.png)

**Start the Virtual Machine**

Press the "Start" arrow to boot the Linux Virtual Machine.

![VM Step8](/Setup_virtual_machine/Screenshots/vm_step8.png)

**Test your OSGeoLive Virtual Machine**

If you follow all the steps correctly the OSGeoLive Virtual Machine you should
pop-up in the Virtual Box window showing something like this:

![VM Step9](/Setup_virtual_machine/Screenshots/vm_step9.png)

**Setting you keyboard layout**

If are not use the US keyboard you have to add your keyboard layout to the bottom
menubar. Therefore on the left bottom corner write “keyboard” in the search box,
and select Keyboard and Mouse.

![VM Step10](/Setup_virtual_machine/Screenshots/vm_step10.png)

Then select “Keyboard Layout” > “Add” and select your keyboard layout in accordance
to your country and language.

![VM Step11](/Setup_virtual_machine/Screenshots/vm_step11.png)

Your keyboard layout will appear as below. Move up to select it as default keyboard layout.

![VM Step12](/Setup_virtual_machine/Screenshots/vm_step12.png)

Open the terminal (black icon in the down left corner of the menubar, close to
the firefox icon) and test if the keyboard layout is correct.


**Screen size/resolution of your OSGeoLive Virtual Machine**

If the screen is very small try to enlarge, and if the enlargement is not working
properly try to reboot. Sometime the Virtual Box guest edition is not kick-in
so you have to do the procedure described below.

Open the bash terminal and run line by line the following codes. The sudo password is “user”.
For security what you type is not shown, anyway it is recorded. After typed the password press enter.

Update the OS. This operation can last few minutes. Be patient. If during the
installation, some screen pop-up asking some question just accept the default option.

```bash
sudo apt update      # update the repositories
sudo apt upgrade -y  # installation of the sw
```

Add to VirtualBox additional add-on to improve the graphical user interface of
the VM. If during the installation some screen pop-up asking some question just
accept the default option.

From the VirtualBox menu press Device > Insert Guest Addition CD image.

![VM Step13](/Setup_virtual_machine/Screenshots/vm_step13.png)

The download procedure will start and a screen will pop up:

![VM Step14](/Setup_virtual_machine/Screenshots/vm_step14.png)

press “insert”, and if the below screen pop up press "cancel"

![VM Step15](/Setup_virtual_machine/Screenshots/vm_step15.png)

Open the terminal and type:

```bash
cd /media/user/VBox_GAs_*
sudo ./VBoxLinuxAdditions.run
```
At this point you can reboot your machine. Now all screen setting, screen scale
and drag/drop should work properly.

<!--
**Test your shared folder**

Another test that you should do, is to see if the shared folder is correctly done.
Open a bash terminal and run

```bash
ls /media/sf_LVM_shared
```

If are able to list the folder then means that the sharing folder operation is
properly done. Moreover try to insert a file from your host OS in the LVM_shared
folder and see if visible in the OSGeoLive.

If you get an error “ls: cannot access ‘/media/sf_LVM_shared’: No such file or directory”
means that you did not correctly done the sharing folder operation so try to redo it.

Test R studio

```bash
rstudio
```

You should see the rstudio software pop-up.

# Git Setting

Follow the instructions below to retrieve the material for the course from our
GitLab repository.

Open the terminal and copy-paste the commands below.

```bash
git clone https://gitlab.com/mueblacker/SIL2022_spatial_analyses.git # only run the first time

# Common practice to separate source and working copies
cp -r ~/SIL2022_spatial_analyses /media/sf_LVM_shared/SIL
```

Run to update the course material:

```bash
cd ~/SIL2022_spatial_analyses
git pull
rsync -hvrPt --ignore-existing ~/SIL2022_spatial_analyses/* /media/sf_LVM_shared/SIL
```
Now you should be ready to follow the workshop.
-->
