#!/bin/sh

##  Basin of interest ID= 1173421
##  Located in Computational Unit = 59

export CU=59
export BA=1173421


#export OUTDATA=/home/jaime/Code/SIL2022_spatial_analyses/data
export DATA=/mnt/shared/Hydrography90m_v1_2022_all_data/
export SIL=/data/marquez/SIL

## extracting basin of interest

    # create mask of macrobasin of interest based on the low resolution file
    pksetmask -co COMPRESS=LZW -co ZLEVEL=9 -ot UInt32 \
        -i $DATA/lbasin_compUnit_tiles/bid${CU}.tif \
        -m $DATA/lbasin_compUnit_tiles/bid${CU}.tif \
        --operator='!' --msknodata ${BA} \
        --nodata 0 -o $SIL/bid_${CU}_bin.tif
    
    # poligonize raster to get extent of basins
    gdal_polygonize.py -8 -f "GPKG" $SIL/bid_${CU}_bin.tif \
       $SIL/bid_${CU}_bin.gpkg

    # extract extent and save
    EXTENSION=$( ogrinfo -so -al $SIL/bid_${CU}_bin.gpkg \
        | grep Extent | grep -Eo '[+-]?[0-9]+([.][0-9]+)?' )
    ulx=$( echo $EXTENSION | awk '{print $1}' )
    uly=$( echo $EXTENSION | awk '{print $4}' )
    lrx=$( echo $EXTENSION | awk '{print $3}' )
    lry=$( echo $EXTENSION | awk '{print $2}' )



    pkcrop  -co COMPRESS=LZW -co ZLEVEL=9 -nodata 0 \
        -i $SIL/bid_${CU}_bin.tif \
        -ulx $ulx -uly $uly -lrx $lrx -lry $lry -o $SIL/basin_${BA}.tif

    gdal_edit.py -a_nodata 0  $SIL/basin_${BA}.tif

    ###########################################################################
    ###########################################################################

    ##   DONE in GRACE
gdal3
pktools

    ulx=6.549167
    uly=46.632500
    lrx=12.367500
    lry=43.996667

## path to CHELSA
mkdir -p $HOME/SIL/CHELSA

export CHELSA=/gpfs/gibbs/pi/hydro/hydro/dataproces/CHELSA/envicloud/chelsa/chelsa_V2/GLOBAL/climatologies/1981-2010/bio/

for f in $(ls $CHELSA)
do
    NA=$(gdalinfo $CHELSA/$f | awk -F"=" '/NoData/ {print $2}')    
    
    pkcrop  -co COMPRESS=LZW -co ZLEVEL=9 -nodata $NA \
        -i  $CHELSA/$f \
        -ulx $ulx -uly $uly -lrx $lrx -lry $lry -o $HOME/SIL/CHELSA/$f

    gdal_edit.py -a_nodata $NA  $HOME/SIL/CHELSA/$f
done

mkdir -p $HOME/SIL/ESALC

export ESALC=/gpfs/gibbs/pi/hydro/hydro/dataproces/ESALC/input

for f in $(ls $ESALC)
do
    NA=$(gdalinfo $ESALC/$f | awk -F"=" '/NoData/ {print $2}')    
    
    pkcrop  -co COMPRESS=LZW -co ZLEVEL=9 -nodata $NA \
        -i  $ESALC/$f \
        -ulx $ulx -uly $uly -lrx $lrx -lry $lry -o $HOME/SIL/ESALC/$f

    gdal_edit.py -a_nodata $NA  $HOME/SIL/ESALC/$f
done

    ###########################################################################
    ###########################################################################

## path to streams
#export STREAM=/mnt/shared/hydrography90m_v1_2022_all_data/stream_tiles_final20d_1p/all_streams_uniqID.vrt
export STREAM=/mnt/shared/hydrography90m_v.1.0_online/hydrography90m_v.1.0/global/segment_ovr.tif

    pkcrop  -co COMPRESS=LZW -co ZLEVEL=9 -nodata 0 \
        -i $STREAM \
        -ulx $ulx -uly $uly -lrx $lrx -lry $lry -o $SIL/stream_${BA}.tif
    gdal_edit.py -a_nodata 0  $SIL/stream_${BA}.tif

## path to subcatchments
export SUBCATCHMENT=$DATA/CompUnit_basin_lbasin_clump_reclas/basin_lbasin_clump_${CU}.tif

    pkcrop  -co COMPRESS=LZW -co ZLEVEL=9 -nodata 0 \
        -i $SUBCATCHMENT \
        -ulx $ulx -uly $uly -lrx $lrx -lry $lry -o $SIL/subcatchment_${BA}.tif
    gdal_edit.py -a_nodata 0  $SIL/subcatchment_${BA}.tif

## path to direction
export DIRECTION=$DATA/CompUnit_dir/dir_${CU}_msk.tif

    pkcrop  -co COMPRESS=LZW -co ZLEVEL=9 -nodata -10 \
        -i $DIRECTION \
        -ulx $ulx -uly $uly -lrx $lrx -lry $lry -o $SIL/direction_${BA}.tif
    gdal_edit.py -a_nodata -10  $SIL/direction_${BA}.tif


    ###########################################################################
    ###########################################################################


    export TOPO=(elev flowpos flow changraddwseg changradupseg changradupcel chancurv  chanelvdwseg chanelvupseg chanelvupcel chanelvdwcel chandistdwseg chandistupseg chandistupcel strdistupnear strdiffupnear strdistupfarth strdiffupfarth strdistdwnear strdiffdwnear outdistdwbasin outdiffdwbasin outdistdwscatch outdiffdwscatch strdistprox slopcmax slopcmin slopdiff slopgrad cti spi sti)

#export VAR=${TOPO["$SLURM_ARRAY_TASK_ID"]}

for VAR in ${TOPO[@]}
do

###################################
##  ELEVATION AND FLOW ACCUMULATION
##################################

    if [ "$VAR" == "elev" ]; then VARINPUT=$DATA/CompUnit_elv/elv_${CU}_msk.tif; fi
    if [ "$VAR" == "flowpos" ]; then VARINPUT=$DATA/CompUnit_flow_pos_noenlarge/flow_${CU}_msk.tif; fi
    if [ "$VAR" == "flow" ]; then VARINPUT=$DATA/CompUnit_flow/flow_${CU}_msk.tif; fi

#############################
##  CHANNEL RELATED VARIABLES
############################

# gradient=channel_grad_dw_seg / Segment downstream (between current cell and the join/outlet) 
    if [ "$VAR" == "changraddwseg" ]; then VARINPUT=$DATA/CompUnit_stream_channel/channel_grad_dw_seg/channel_grad_dw_seg_${CU}.tif; fi 

# gradient=channel_grad_up_seg / Segment upstream (between current cell and the init/join)
    if [ "$VAR" == "changradupseg" ]; then VARINPUT=$DATA/CompUnit_stream_channel/channel_grad_up_seg/channel_grad_up_seg_${CU}.tif; fi

# gradient=channel_grad_up_cel / Cell upstream (between current cell and next cell) 
    if [ "$VAR" == "changradupcel" ]; then VARINPUT=$DATA/CompUnit_stream_channel/channel_grad_up_cel/channel_grad_up_cel_${CU}.tif; fi

# curvature=channel_curv_cel / Cell stream course curvature (current cell) 
    if [ "$VAR" == "chancurv" ]; then VARINPUT=$DATA/CompUnit_stream_channel/channel_curv_cel/channel_curv_cel_${CU}.tif; fi

# difference=channel_elv_dw_seg
    if [ "$VAR" == "chanelvdwseg" ]; then VARINPUT=$DATA/CompUnit_stream_channel/channel_elv_dw_seg/channel_elv_dw_seg_${CU}.tif; fi

# difference=channel_elv_up_seg / Segment upstream (between current cell and the init/join) 
    if [ "$VAR" == "chanelvupseg" ]; then VARINPUT=$DATA/CompUnit_stream_channel/channel_elv_up_seg/channel_elv_up_seg_${CU}.tif; fi

# difference=channel_elv_up_cel / Cell upstream (between current cell and next cell) 
    if [ "$VAR" == "chanelvupcel" ]; then VARINPUT=$DATA/CompUnit_stream_channel/channel_elv_up_cel/channel_elv_up_cel_${CU}.tif; fi

# difference=channel_elv_dw_cel / Cell downstream (between current cell and next cell) 
    if [ "$VAR" == "chanelvdwcel" ]; then VARINPUT=$DATA/CompUnit_stream_channel/channel_elv_dw_cel/channel_elv_dw_cel_${CU}.tif; fi

# distance=channel_dist_dw_seg / Segment downstream (between current cell and the join/outlet)
    if [ "$VAR" == "chandistdwseg" ]; then VARINPUT=$DATA/CompUnit_stream_channel/channel_dist_dw_seg/channel_dist_dw_seg_${CU}.tif; fi

# distance=channel_dist_up_seg / Segment upstream (between current cell and the init/join) 
    if [ "$VAR" == "chandistupseg" ]; then VARINPUT=$DATA/CompUnit_stream_channel/channel_dist_up_seg/channel_dist_up_seg_${CU}.tif; fi

# distance=channel_dist_up_cel / Cell upstream (between current cell and next cell) 
    if [ "$VAR" == "chandistupcel" ]; then VARINPUT=$DATA/CompUnit_stream_channel/channel_dist_up_cel/channel_dist_up_cel_${CU}.tif; fi

#############################
##  DISTANCE RELATED VARIABLES
############################

# distance=distance_stream_upstream / Distance of the shortest path from a stream pixel to the divide
    if [ "$VAR" == "strdistupnear" ]; then VARINPUT=$DATA/CompUnit_stream_dist/stream_dist_up_near/stream_dist_up_near_${CU}.tif; fi

# difference=difference_stream_upstream / Elevation difference of the shortest path from a stream pixel to the divide
    if [ "$VAR" == "strdiffupnear" ]; then VARINPUT=$DATA/CompUnit_stream_dist/stream_diff_up_near/stream_diff_up_near_${CU}.tif; fi

# distance=distance_stream_upstream / Distance of the longest path from a stream pixel to the divide
    if [ "$VAR" == "strdistupfarth" ]; then VARINPUT=$DATA/CompUnit_stream_dist/stream_dist_up_farth/stream_dist_up_farth_${CU}.tif; fi

# difference=difference_stream_upstream / Elevation difference of the longest path from a stream pixel to the divide
    if [ "$VAR" == "strdiffupfarth" ]; then VARINPUT=$DATA/CompUnit_stream_dist/stream_diff_up_farth/stream_diff_up_farth_${CU}.tif; fi

# distance=distance_stream_downstream / Distance of the lngest pathfrom the divide to reach a stream pixel
    if [ "$VAR" == "strdistdwnear" ]; then VARINPUT=$DATA/CompUnit_stream_dist/stream_dist_dw_near/stream_dist_dw_near_${CU}.tif; fi

# difference=difference_stream_downstream / Elevation difference of the longest path from the divide to reach a stream pixel
    if [ "$VAR" == "strdiffdwnear" ]; then VARINPUT=$DATA/CompUnit_stream_dist/stream_diff_dw_near/stream_diff_dw_near_${CU}.tif; fi

# distance=distance_stream_downstream / Distance of the longest path from the divide to reach an outlet pixel
    if [ "$VAR" == "outdistdwbasin" ]; then VARINPUT=$DATA/CompUnit_stream_dist/outlet_dist_dw_basin/outlet_dist_dw_basin_${CU}.tif; fi

# difference=difference_stream_downstream / Elevation difference of the longest path from the divide to reach an outlet pixel
    if [ "$VAR" == "outdiffdwbasin" ]; then VARINPUT=$DATA/CompUnit_stream_dist/outlet_diff_dw_basin/outlet_diff_dw_basin_${CU}.tif; fi

# distance=distance_stream_downstream / Distance of the longest path from the divide to reach a stream node pixel
    if [ "$VAR" == "outdistdwscatch" ]; then VARINPUT=$DATA/CompUnit_stream_dist/outlet_dist_dw_scatch/outlet_dist_dw_scatch_${CU}.tif; fi

# difference=difference_stream_downstream / Elevation difference of the longest path from the divide to reach a stream node pixel
    if [ "$VAR" == "outdiffdwscatch" ]; then VARINPUT=$DATA/CompUnit_stream_dist/outlet_diff_dw_scatch/outlet_diff_dw_scatch_${CU}.tif; fi

# distance=streams_proximity / Euclidean distance from the streams
    if [ "$VAR" == "strdistprox" ]; then VARINPUT=$DATA/CompUnit_stream_dist/stream_dist_proximity/stream_dist_proximity_${CU}.tif; fi

#############################
##  SLOPE RELATED VARIABLES
############################
    
# maxcurv=slope_curv_max_dw_cel /  Cell (between highest upstream cell, current cell anddownstream cell) 
    if [ "$VAR" == "slopcmax" ]; then VARINPUT=$DATA/CompUnit_stream_slope/slope_curv_max_dw_cel/slope_curv_max_dw_cel_${CU}.tif; fi

# mincurv=slope_curv_min_dw_cel / Cell (between lowest upstream cell, current cell anddownstream cell)
    if [ "$VAR" == "slopcmin" ]; then VARINPUT=$DATA/CompUnit_stream_slope/slope_curv_min_dw_cel/slope_curv_min_dw_cel_${CU}.tif; fi
   
# difference=slope_elv_dw_cel / Cell (between current cell anddownstream cell) 
    if [ "$VAR" == "slopdiff" ]; then VARINPUT=$DATA/CompUnit_stream_slope/slope_elv_dw_cel/slope_elv_dw_cel_${CU}.tif; fi
   
# gradient=slope_grad_dw_cel / Cell gradient (elevation differencedivided by distance) 
    if [ "$VAR" == "slopgrad" ]; then VARINPUT=$DATA/CompUnit_stream_slope/slope_grad_dw_cel/slope_grad_dw_cel_${CU}.tif; fi

############################
##  STREAM FLOW INDICES 
############################

    if [ "$VAR" == "cti" ]; then VARINPUT=$DATA/CompUnit_stream_indices_tiles20d/all_tif_cti_dis.vrt; fi
    if [ "$VAR" == "spi" ]; then VARINPUT=$DATA/CompUnit_stream_indices_tiles20d/all_tif_spi_dis.vrt; fi
    if [ "$VAR" == "sti" ]; then VARINPUT=$DATA/CompUnit_stream_indices_tiles20d/all_tif_sti_dis.vrt; fi

############################
##  GARID (aridity) & GEVAPT (evapotranspiration) 
############################

#    if [ "$VAR" == "cti" ]; then VARINPUT=$DATA/CompUnit_stream_indices_tiles20d/all_tif_cti_dis.vrt; fi
#    if [ "$VAR" == "spi" ]; then VARINPUT=$DATA/CompUnit_stream_indices_tiles20d/all_tif_spi_dis.vrt; fi


    NA=$(gdalinfo $VARINPUT | awk -F"=" '/NoData/ {print $2}')    
    pkcrop  -co COMPRESS=LZW -co ZLEVEL=9 -nodata $NA \
        -i $VARINPUT \
        -ulx $ulx -uly $uly -lrx $lrx -lry $lry -o $SIL/${VAR}_${BA}.tif
    gdal_edit.py -a_nodata $NA  $SIL/${VAR}_${BA}.tif

done

###############################################################################
###############################################################################

####    Get the vector stream network

time ogr2ogr \
    $SIL/order_vect_${CU}.gpkg \
    $DATA/CompUnit_stream_order/vect/order_vect_${CU}.gpkg \
    -spat $ulx $lry $lrx $uly \
    -sql "SELECT * FROM vect  WHERE ST_GeometryType(geom) LIKE 'LINESTRING'"


