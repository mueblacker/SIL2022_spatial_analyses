# Spatial freshwater biodiversity analyses

Freshwater habitats are strongly characterized by the connectivity among water
bodies, yet this unique feature is often neglected in freshwater biodiversity
analyses. In this interactive workshop we will focus on species geographic data
and how this can be linked to a high-resolution hydrographic network to append
spatially explicit attributes regarding connectivity, network topology,and
environmental data. We will provide a full workflow starting with raw GPS data and use a suite of open-source tools including GRASS-GIS, GDAL, and R and we will run a simple species distribution model to project the suitable habitat of a given
species across the study region. 


# Biodiversity modeling in freshwater systems

## Freshwater biodiversity decline
![Freshwater biodiversity decline](/figures/freshwater_bio_decline.png)

## Wallacean shortfall
 - Limited description of patterns and processes: Geographical distributions of
 most species are poorly understood and contain many gaps
 - Data scarceness: Lack of adequate sampling efforts across multiple regions

## Possible solution
![Species distribution modeling (SDM)](/figures/SDM.png)


## GIS tools and R implementations

![GIS tools](/figures/GIS_tools.png)


### Links

**BASH command line**

[GDAL](https://gdal.org/): 'Geospatial' Data Abstraction Library.  \
[GEOS](https://libgeos.org/): Library for computational geometry.  \
[PROJ](https://proj.org/): Generic coordinate transformation software. \
[GRASS GIS](https://grass.osgeo.org/): Powerful raster, vector, and geospatial
processing engines. \
[pktools](http://pktools.nongnu.org/): Suite of utilities written in C++ for
image processing with a focus on remote sensing applications.

**R packages**

*Spatial data analysis*

[rgrass7](https://cran.r-project.org/web/packages/rgrass7/index.html): Interface
Between GRASS Geographical Information System and R.

[sf](https://r-spatial.github.io/sf/): Simple Features for R represents simple
features as records in a data.frame or tibble with a geometry list-column.\
[stars](https://r-spatial.github.io/stars/): Classes and methods for reading,
manipulating, plotting and writing data cubes.

[terra](https://rspatial.github.io/terra/reference/terra-package.html): Spatial
data analysis

[raster](https://rspatial.org/raster): Geographic data analysis and modeling \
[sp](https://cran.r-project.org/web/packages/sp/index.html): Classes and methods
for spatial data \
[rgdal](https://cran.r-project.org/web/packages/rgdal/index.html): Bindings for
the 'Geospatial' Data Abstraction Library \
[rgeos](https://cran.r-project.org/web/packages/rgeos/index.html):Interface to
Geometry Engine - Open Source ('GEOS')

**Additional R packages**

*Data manipulation and analyses*

[data.table](https://cran.r-project.org/web/packages/data.table/index.html): Fast
aggregation of large data.

[tidyverse](https://www.tidyverse.org/packages/): Collection of R packages, which
are commonly used for data analyses including [readr](https://readr.tidyverse.org/)
for a fast data reading, [tidyr](https://tidyr.tidyverse.org/) for tiding your data,
[dplyr](https://dplyr.tidyverse.org/) for data manipulation,
[ggplot2](https://ggplot2.tidyverse.org/) for creating graphics,
[purrr](https://purrr.tidyverse.org/) for working with functions and vectors,
[forcats](https://forcats.tidyverse.org/) for solving common problems with factors,
[stringr](https://stringr.tidyverse.org/) for data cleaning and preparation tasks,
and [tibble](https://tibble.tidyverse.org/) modern re-imagining of the data.frame.

[stringi](https://stringi.gagolewski.com/): Character string processing facilities. \
[ids](https://cran.r-project.org/web/packages/ids/vignettes/ids.html): Provides
randomly generated ids in a number of different forms with different readability
and sizes.

*Controlling system processes*

[processx](https://processx.r-lib.org/): Execute and control system processes -
tools to run system processes in the background.

*Machine learning*

[ranger](https://github.com/imbs-hl/ranger): A fast implementation of Random
Forests, particularly suited for high dimensional data.\
[vimp](https://bdwilliamson.github.io/vimp/): Perform inference on algorithm-agnostic
variable importance.


**Additional information**

[Overview of R packages for spatial data](https://cran.r-project.org/web/views/Spatial.html) \
[Geocomputation with R](https://geocompr.robinlovelace.net/intro.html#the-history-of-r-spatial) \
[r-spatial](https://r-spatial.org/)


## Datasets

### Species data

![EPTO Database](/figures/EPTO.png)

We use species occurrence data from the new EPTO database (Grigoropoulou et al., _in review_).

Check out also the online visualization of the data in the IGB GeoNode at <https://geo.igb-berlin.de/maps/621/view> 



### Environmental variables

#### Stream network

**Hydrography90m**

![Hydrography90m - Drainage basins ](/figures/hydrography90m_geonode.png)

We use the new Hydrography90m dataset by [Amatulli et al. (2022)](https://essd.copernicus.org/preprints/essd-2022-9). This is a high-resoluton hydrographic network that comes with a suite of topologic and topographic properties at a spatial resolution of 3 arc sec (~90m) globally.
In total, Hydrography90m contains 1 560 488 drainage basins and 720M stream reaches and sub-catchments. 
You can download the data at <https://hydrography.org/> and visualize it at <https://geo.igb-berlin.de/maps/351/view>

The Hydrography90m dataset is derived from the Multi-Error-Removed Improved-Terrain Digital Elevation Model (MERIT-DEM) by [Yamazaki et al. (2017)](https://agupubs.onlinelibrary.wiley.com/doi/full/10.1002/2017GL072874), likewise at a spatial resolution of 3 arc sec (~90m). You can download the MERIT-DEM at <http://hydro.iis.u-tokyo.ac.jp/~yamadai/MERIT_DEM/>.


##### Merging and cropping of Hydrography90m layers

The example code shows how tiles of the [Hydrography90m](https://hydrography.org/) dataset can be merged and
the sub-catchment raster layer can be cropped by the drainage basin ID.


```r
# Libraries
library(tidyverse)
library(terra)
library(sf)

# Load data

# Vector files
# Basin
bsn_vct_h18v02 <- read_sf("/home/user/SIL/data/basin_tiles20d/basin_h18v02.gpkg")
bsn_vct_h18v04 <- read_sf("/home/user/SIL/data/basin_tiles20d/basin_h18v04.gpkg")

# bsn_vct_h18v02
#  Simple feature collection with 48924 features and 1 field
# Geometry type: POLYGON
# Dimension:     XY
# Bounding box:  xmin: 0 ymin: 45 xmax: 20 ymax: 65
# Geodetic CRS:  WGS 84
# A tibble: 48,924 × 2
#        ID                                                                                         geom
#     <int>                                                                                <POLYGON [°]>
# 1 1285727 ((10.90083 65, 10.90083 64.99917, 10.9 64.99917, 10.9 64.99583, 10.90417 64.99583, 10.904...
# 2 1210686 ((11.62583 65, 11.62583 64.99833, 11.625 64.99833, 11.625 64.9975, 11.62667 64.9975, 11.6...
# 3 1230096 ((11.73 65, 11.73 64.99917, 11.73083 64.99917, 11.73083 64.9975, 11.73167 64.9975, 11.731...
# … with 48,921 more rows

# Raster files
# Basin
bsn_rst_h18v02 <- rast("/home/user/SIL/data/basin_tiles20d/basin_h18v02.tif")
bsn_rst_h18v04 <- rast("/home/user/SIL/data/basin_tiles20d/basin_h18v04.tif")

# bsn_rst_h18v02
# class       : SpatRaster 
# dimensions  : 24000, 24000, 1  (nrow, ncol, nlyr)
# resolution  : 0.0008333333, 0.0008333333  (x, y)
# extent      : 0, 20, 45, 65  (xmin, xmax, ymin, ymax)
# coord. ref. : lon/lat WGS 84 (EPSG:4326) 
# source      : basin_h18v02.tif 
# name        : basin_h18v02 

# Sub-catchment
subc_rst_h18v02 <- rast("/home/user/SIL/data/sub_catchment_tiles20d/sub_catchment_h18v02.tif")
subc_rst_h18v04 <- rast("/home/user/SIL/data/sub_catchment_tiles20d/sub_catchment_h18v04.tif")

```

The selected drainage basin extends over two tiles. Therefore, the tiles need to
be merged first and then a mask can be created using the basin ID. The mask can
be created using the gpkg file or the raster file as a input layer.


```r
# Merge the tiles and create mask for cropping

# Select basin IDs
# also multiple IDs can be selected e.g. c(1173421, 1173422, ...)
basin_id <- 1173421

# Create basin mask from vector file
# Filter all IDs listed in basin_id
# Make an invalid geometry valid
# Group rows by ID
# Rejoin the polygon from the two tiles by summarizing each group
bsn_vct_1173421 <- bind_rows(bsn_vct_h18v02, bsn_vct_h18v04) %>%
  filter(ID %in% basin_id) %>%
  st_make_valid() %>%
  group_by(ID) %>%
  summarise(., ID = ID[1], .groups = "drop")



# Create basin mask from raster file
# Merge tiles With the function 'merge'  
# Use the function 'mosaic' in the case of overlapping layers
# Create mask with classify: cells with the basin_id become 1 and all other
# cells become NA
bsn_rst_1173421 <- terra::merge(bsn_rst_h18v02, bsn_rst_h18v04) %>%
 classify(., matrix(c(basin_id, 1), ncol = 2), others = NA)

# Merge tiles of the sub-catchment layers
subc_rst_h18 <- terra::merge(subc_rst_h18v02, subc_rst_h18v04)
```
Now the masks can be used to crop the sub-catchment raster layer.

```r
# Crop layers by the extend of the basin

# Using a vector file
subc_rst_1173421v <- crop(subc_rst_h18, vect(bsn_vct_1173421), mask=TRUE)

# Using a raster file
# Multiple steps are needed
# First: Cells outside of the drainage basin need to be set to NA
# This can be done by multiplying the mask layer with the sub-catchment layer
subc_rst_1173421 <- bsn_rst_1173421 * subc_rst_h18

# Second: Pre-crop raster to reduce the run time of the function 'trim'
extend <- ext(6.5, 12.4, 43.9, 46.7)
subc_rst_1173421 <- crop(subc_rst_1173421, extend)
# Third: Trim sub-catchment layer by removing outer rows and columns that are NA
subc_rst_1173421 <- trim(subc_rst_1173421)
```

#### CHELSA Bioclim

Bioclimatic variables are derived variables from the monthly mean, max,
mean temperature, and mean precipitation values.

<https://chelsa-climate.org/>

- Time range: 1981-2010
- Temporal resolution: Long term average
- Spatial resolution: 30 arc sec, ~1km

|Variable|Description|Unit|Scale|Offset|
|---|---|---|---|---|
|bio1|Annual mean temperature|°C|0.1|-273.15|
|bio2|Mean diurnal range|°C|0.1||
|bio3|Isothermality|°C|0.1||
|bio4|temperature seasonality|°C|0.1||
|bio5|Max temperature of warmest Month|°C|0.1|-273.15|
|bio6|Min temperature of coldest Month|°C|0.1|-273.15|
|bio7|Temperature annual range|°C|0.1||
|bio8|Mean temperature of wettest quarter|°C|0.1|-273.15|
|bio9|Mean temperature of driest quarter|°C|0.1|-273.15|
|bio10|Mean temperature of warmest quarter|°C|0.1|-273.15|
|bio11|Mean temperature of coldest quarter|kg/m²|0.1|-273.15|
|bio12|Annual precipitation|kg/m²|0.1||
|bio13|Precipitation of wettest month|kg/m²|0.1||
|bio14|Precipitation of driest month|kg/m²|0.1||
|bio15|Precipitation seasonality|kg/m²|0.1||
|bio16|Precipitation of wettest quarter|kg/m²|0.1||
|bio17|Precipitation of driest quarter|kg/m²|0.1||
|bio18|Precipitation of warmest quarter|kg/m²|0.1||
|bio19|Precipitation of coldest quarter|kg/m²|0.1||


## Processing of spatial data

### Aggregating of environmental variables by sub-catchments

Additional explanatory variables can be calculated if the values of any particular environmental
layer are summarized within a subcatchment. This is possible if the subcatchments
are the units of analysis and if the pixel resolution of the environmental layers
is smaller than the area of each subcatchment

![Variable aggregation](/figures/var_aggregation.png)

We have aggregated 726 million unique subcatchments globally and calculated the
following statistics:

- Mean
- Standard Deviation
- Minimum
- Maximum
- Range

This has been done with the Hydrography90m layers, Land cover, Soils and 
climate data.

The main tool for this process has been the *r.univar* command of GRASS GIS

```
grass78  -f -text --tmp-location  -c msk.tif

#  Read files with subcatchments
r.in.gdal input=subcatchments.tif output=micb 

# Read environmental layer
  r.external input=elevation.tif output=elev
  
# Prepare output file with headers
  echo "subcID min max range mean sd" > stats_out.txt  

# Calculate statistics and append to output file
  r.univar -t map=elev zones=micb separator=space | \
      awk 'NR > 1 {print $1, $4, $5, $6, $7, $9}' \
      >> stats_out.txt
```

## Description of the study case

### Location

![Location study case](/figures/location_study_case.png)

![Elevation](/figures/elv_strm.png)

![bio1](/figures/bio1_strm.png)


Figure: Po river basin maps showing: stream network and sub-catchment,
elevation, temperature, precipitation, land cover



## Workflow

![Workflow](/figures/workflow.png)

# Follow-along application

## Setting the scene

The initial step is to load all necessary packages

```r
if (!require("processx")) { install.packages("processx", dependencies = TRUE) ; library(processx)}
if (!require("dplyr")) { install.packages("dplyr", dependencies = TRUE) ; library(dplyr)}
if (!require("data.table")) { install.packages("data.table", dependencies = TRUE) ; library(data.table)}
if (!require("raster")) { install.packages("raster", dependencies = TRUE) ; library(raster)}
if (!require("stringi")) { install.packages("stringi", dependencies = TRUE) ; library(stringi)}
if (!require("ids")) { install.packages("ids", dependencies = TRUE) ; library(ids)}
if (!require("ranger")) { install.packages("ranger", dependencies = TRUE) ; library(ranger)}
if (!require("vip")) { install.packages("vip", dependencies = TRUE) ; library(vip)}

if (!require("viridis")) { install.packages("viridis", dependencies = TRUE) ; library(viridis)}
if (!require("igraph")) { install.packages("igraph", dependencies = TRUE) ; library(igraph)}
if (!require("RSQLite")) { install.packages("RSQLite", dependencies = TRUE) ; library(RSQLite, quietly=T)}
if (!require("intergraph")) { install.packages("intergraph", dependencies = TRUE) ; library(intergraph)}
if (!require("rgl")) { install.packages("rgl", dependencies = TRUE) ; library(rgl)}
if (!require("network")) { install.packages("network", dependencies = TRUE) ; library(network)}
if (!require("sna")) { install.packages("sna", dependencies = TRUE) ; library(sna)}


options(scipen=999) # avoid any exponential numbers in the reclassification



```

The R functions presented here are wrapping functions of different bash scripts.
Therefore, we first need to make the bash scripts executable

```r
# make the bash scripts executable
scripts <- list.files("~/SIL/Scripts/", pattern = "\\.sh", full.names = T)
lapply(scripts, function(x) system(command = paste0("chmod u+x ", x)))
```

Set path to folders for input, output and temporal data

```r
data_dir <- "~/SIL/data/"
data_out <- "~/SIL/output/"
data_tmp <- "~/SIL/tmp/"
```

## Taxon data

In the *data* folder we provide a file *EPT_selected_SIL_workshop.csv* with the
information for several EPT taxa.

Import occurrence records:
```r
data <- fread(paste0(data_dir, "EPT_selected_SIL_workshop.csv"), fill = T,
          quote = "", header = T, sep="\t")
colnames(data)
#  [1] "order_ID"              "dataset_ID"            "occurrence_ID"        
#  [4] "species"               "genus"                 "family"               
#  [7] "order"                 "longitude"             "latitude"             
# [10] "no_decimals_published" "no_decimals_available" "location_river"       
# [13] "country"               "sampling_day"          "month"                
# [16] "year"                  "source"                "FRED_link.DOI"        
# [19] "notes"                 "other_info"            "cap_fl"               
# [22] "cen_fl"                "coun_fl"               "snapped"              
# [25] "subcatchment_ID"       "basin_ID"              "elevation"   
```



Select the taxon of interest:
```r

participant <- 6
taxon <- unique(data$genus)[participant]
taxon
# "Consorophylax"
```

Select occurrence points of the specific genus:
```r
data <- data[data$genus==taxon, c("occurrence_ID","genus","longitude","latitude")]
```

Add a column indicating presence
```r
data$occurrence <- 1
head(data)
#     occurrence_ID         genus longitude latitude occurrence
# 622    1000181838 Consorophylax   7.16667  44.2500          1
# 623    1000181839 Consorophylax   8.00000  45.0000          1
# 624    1000181840 Consorophylax   8.00000  45.0000          1
# 625    1000181841 Consorophylax   9.91667  46.1833          1
# 626    1000181842 Consorophylax  10.48330  46.3333          1
# 627    1001790957 Consorophylax   8.70919  46.5167          1
```
------------------------

Export occurrence points of the specific genus in a space-separated file
```r
fwrite(data, paste0(data_out, "/", taxon, "_points.txt"), quote = F,
                  sep = " ",  row.names = F)
```
-------------------------

## Extracting sub-catchment IDs

As preparation for the modelling part we need to create a table consisting of
the values of all environmental variables occurring at the location of the
presences. In our case the locations or units of analysis
are the subcatchments (and not the pixels).

![picture 12](figures/screen04.png)  


Therefore, the first step is to overlay the occurrence locations with the
subcatchment layer to identify the subcatchment ID for each location

Import basin and subcatchment raster files:
```r
## Read basin file
basin <- raster(paste0(data_dir, "basin_1173421.tif"))

## Read subcatchment file
subc <- raster(paste0(data_dir, "subcatchment_1173421.tif"))
```
-------------------
Visualise occurrence points:
```r
# First mask subcatchment raster with the basin raster
subc_msk <- mask(subc, basin)
# Plot
plot(subc_msk)
points(data[,c('longitude','latitude')], cex = 2, col = "red", pch = 16)
```
-------------------
![picture 3](figures/subc_points.png)  

Before running the R function to extract the subcatchment IDs, let's look at the
bash script. The *gdallocationinfo()* function queries information about a pixel given its location.

```bash
#!/bin/bash

export DATA=$1
export SUBC=$2

awk 'FNR > 1 {print $3, $4}' $DATA   | gdallocationinfo -valonly -geoloc  $SUBC
```
-------------------
Here is the R function that wraps the bash script:

```r
get_subcID <- function(taxon, subc_path, dataset_path) {

  dataIDs <- NULL
  IDs <- NULL

  # Call external gdal command of gdallocationinfo()
  IDs <- processx::run(paste0("~/SIL/Scripts/gdallocinfo.sh"),
                       args = c(dataset_path, subc_path),
                       echo = F)$stdout
  # Format the IDs string
  IDs <- data.frame(subcID = strsplit(IDs, "\n", "", fixed = TRUE)[[1]])

  ## Import taxon occurrence points
  dataset <- fread(dataset_path, header = T, sep = " ")

  # Join the IDs with the observations
  dataIDs <- cbind.data.frame(dataset, IDs)

  return(dataIDs)

}
```
-------------------
Finally, call the R function to do the extraction:

```r
dataIDs <- get_subcID(taxon = taxon,
                      subc_path = subc_path,
                      dataset_path = paste0(data_out, taxon, "_points.txt")
                      
)
```
-------------------
Export table with coordinates and IDs

```r
fwrite(dataIDs, paste0(data_out,"/",taxon,"_pres_subcIDs.csv"), row.names = F, sep = " ", quote = F)
```
------------------

## (Pseudo)-Absence data

Import presence data with their subcatchment IDs

```r
pres <- fread(paste0(data_out, taxon, "_pres_subcIDs.csv"), header = T)
head(pres)
#   occurrence_ID         genus longitude latitude occurrence    subcID
# 1    1000181838 Consorophylax   7.16667  44.2500          1 512753882
# 2    1000181839 Consorophylax   8.00000  45.0000          1 512279371
# 3    1000181840 Consorophylax   8.00000  45.0000          1 512279371
# 4    1000181841 Consorophylax   9.91667  46.1833          1 511672795
# 5    1000181842 Consorophylax  10.48330  46.3333          1 511595011
# 6    1001790957 Consorophylax   8.70919  46.5167          1 511515105
```

The values of the subcatchment raster are the unique IDs of the subcatchments.
Take a random sample from the cell values of the raster layer (without replacement).
Also get the coordinates of the subcatchments with the option xy=TRUE.
The occurrence_ID of the pseudoabsences will start with '999'.

```r
pseudo_sample <- function(raster, nsample, taxon_name, ID_length) {

  pseudoabs <- NULL
  pseudo_ID <- NULL

  pseudoabs <- as.data.frame(sampleRandom(raster, nsample, xy = TRUE))

  # Give the pseudoabs dataframe the same format as the presence table
  # 1. Assign random unique ID to each pseudoabsence
  # (Make more random IDs and sample only the required number of unique ones.)
  pseudo_ID <- paste0('999', sample(unique(
    do.call(paste0,
            Map(stri_rand_strings,
                n = nrow(pseudoabs) * 10,
                length=ID_length - 3,
                pattern = "[0-9]"))
  ),
  nrow(pseudoabs)
  ))


  # 2. Create the table
  pseudoabs <- cbind.data.frame(
    occurrence_ID = as.numeric(pseudo_ID),
    genus = rep(taxon_name, nrow(pseudoabs)),
    longitude = pseudoabs[, 1],
    latitude = pseudoabs[, 2],
    occurrence = rep(0, nrow(pseudoabs)),
    subcID = pseudoabs[, 3])


  return(pseudoabs)

}
```
Call the function

```r
pseudo <- pseudo_sample(raster = subc_msk,
                        nsample = 10000,
                        ID_length = stri_length(pres$occurrence_ID[1]),
                        taxon_name= taxon
)

head(pseudo)
#   occurrence_ID         genus longitude latitude occurrence    subcID
# 1    9995115633 Consorophylax  9.916250 46.37042          0 511582898
# 2    9991905417 Consorophylax  8.077917 45.39708          0 512050997
# 3    9999912578 Consorophylax  8.031250 45.15375          0 512180690
# 4    9996002591 Consorophylax  9.348750 45.74208          0 511885265
# 5    9992706421 Consorophylax  8.092084 44.55708          0 512563538
# 6    9990046693 Consorophylax 10.646250 45.75458          0 511879885
```
Finally merge the presence and absence data

```r
#  Merge the presence-pseudoabsence tables
pres_abs <- bind_rows(pres, pseudo)

# Export final table
fwrite(pres_abs, paste0(data_out, taxon, "_pres_abs.csv"),
            sep = " ", row.names = F, quote = F)
```

Visualise

```r
plot(subc_msk)
points(pseudo[,c('longitude','latitude')], cex = 0.01)
points(pres[,c('longitude','latitude')], col = "red", cex=2, pch=16)
```

![picture 2](figures/pseudo_abs.png)  




## Extract environmental predictors

### Elevation, Slope, Flow Accumulation

```r
basinID <- 1173421
dataset_path <- paste0(data_dir, taxon, "_pres_abs.csv")
tmp_path <- paste0(data_dir, "/tmp")
stats_dir <- paste0(data_dir, "stats")
dir.create(stats_dir)

```

```r
## Define rescaling functions
slope_scale <- function(x, na.rm = F) (x*0.000001)
```

```r
env_vars_extract <- function(data_dir, dataset_path, basinID, out_dir) {
    all_data <- NULL
    all_data_rescale <- NULL
    list_df <- NULL

    tmp_path <- paste0(data_out, "/tmp")
    dir.create(tmp_path)

    processx::run(paste0(script_dir, "/home/user/SIL/Scripts/env_layers.sh"),
                  args = c(data_dir, dataset_path, basinID, out_dir),
                  echo = T)

    ## Format output
    env_vars_files <- list.files(paste0(data_out, "stats"),
                                  full.names = T, pattern = "complete")
    # Load data sets
    list_df <- lapply(env_vars_files, function(x)
      fread(x, sep = ",", header = T))
    ## see colnames
    list_df <- lapply(list_df, function(x) subset(x, select = -zone))
    all_data <- Reduce(full_join, list_df)

    # Exclude some predictor columns
    all_data <- all_data %>% dplyr::select(!contains(c("min", "max", "range")))

    ## Rescale slope gradient variable
    all_data_rescale <- all_data  %>%
      mutate(across(contains("slopgrad"), slope_scale))

    ## Rename some columns
    colnames(all_data_rescale) = gsub("stddev", "sd", colnames(all_data_rescale))

    return(all_data_rescale)

}

# Call the function
env_preds <- env_vars_extract(data_dir = data_dir, dataset_path = dataset_path,
                              basinID = basinID, out_dir = stats_dir)

# Export table
fwrite(env_preds, paste0(data_out, taxon, "_env_predictors.csv"),
            row.names = F, quote = F, sep = ",")

```


### Bioclim variables

```r

## Define rescaling functions
clim_scale <- function(x, na.rm = F) (x * 0.1)
offset <- function(x, na.rm = F) (x - 273.15)

bioclim_extract <- function(data_dir, dataset_path, taxon, basinID, out_dir) {
  all_data <- NULL
  all_data_rescale <- NULL
  list_df <- NULL
  
    tmp_path <- paste0(data_out, "/tmp")
    dir.create(tmp_path)

    processx::run("/home/user/SIL/Scripts/Bioclim_layers.sh",
                  args = c(data_dir, dataset_path, tmp_path, basinID, out_dir),
                  echo = T)
  # Load data sets
  bioclim_files <- list.files(out_dir,
                              full.names = T, pattern = "stats_bio")
  list_df <- lapply(bioclim_files, function(x)
    fread(x, sep = " ", header = T))

  ## Join datasets by subcID
  all_data <- Reduce(full_join, list_df)

  # Exclude some predictor columns
  all_data <- all_data %>% dplyr::select(!contains(c("min", "max", "range")))


  all_data_rescale <- all_data  %>%
    mutate(across(starts_with("bio"), clim_scale))  %>%
    mutate(across(contains("mean"), offset))
  
  return(all_data_rescale)

}

# Call the function
bioclim_preds <- bioclim_extract(data_dir = data_dir, dataset_path = dataset_path,
                                 taxon = taxon, basinID = basinID, out_dir = stats_dir)

# Export table
fwrite(bioclim_preds, paste0(data_out, taxon, "_bioclim.csv"),
            row.names = F, quote = F, sep = ",")


dataset <- fread(paste0(data_out, taxon, "_pres_abs.csv"),
                      sep = " ", header = T)
env_preds <- fread(paste0(data_out, taxon, "_env_predictors.csv"),
                      sep = ",", header = T)
bioclim <- fread(paste0(data_out, taxon, "_bioclim.csv"),
                      sep = ",", header = T)
```

## Join presence-absence dataset and  predictors

For merging the subcatchment, the subcatchment ID can be used as the common field

```r
model_data <- Reduce(left_join, list(dataset, env_preds, bioclim))

fwrite(model_data, paste0(data_out, taxon, "_model_data.csv"),
            row.names = F, quote = F, sep = ",")
```

## Modelling

```r
rf_data <- fread(paste0(data_out, taxon, "_model_data.csv"), header = T, sep = ",")
head(rf_data)
#  occurrence_ID         genus longitude latitude occurrence    subcID elev_mean
# 1    1000181838 Consorophylax   7.16667  44.2500          1 512753882 2164.5919
# 2    1000181839 Consorophylax   8.00000  45.0000          1 512279371  241.9105
# 3    1000181840 Consorophylax   8.00000  45.0000          1 512279371  241.9105
# 4    1000181841 Consorophylax   9.91667  46.1833          1 511672795  799.7503
# 5    1000181842 Consorophylax  10.48330  46.3333          1 511595011 2528.6937
# 6    1001790957 Consorophylax   8.70919  46.5167          1 511515105 1261.3279
#     elev_sd  flow_mean    flow_sd slopgrad_mean slopgrad_sd bio10_mean
# 1 233.24352 0.07882409 0.09792604     0.4458533  0.20100088  11.159184
# 2  14.20989 0.02636182 0.03369026     0.1558559  0.06985787  22.802632
# 3  14.20989 0.02636182 0.03369026     0.1558559  0.06985787  22.802632
# 4 391.96020 0.43424951 4.67700022     0.3246312  0.21585665  17.756091
# 5 148.13988 0.07114719 0.13568431     0.3658229  0.24666887   7.121631
# 6 205.89744 0.24408140 0.37563376     0.4387368  0.30421971  14.016279
#     bio10_sd bio11_mean   bio11_sd bio5_mean    bio5_sd  bio6_mean    bio6_sd
# 1 1.63170668  -6.162245 1.59602602  16.68163 1.63486234 -11.262245 1.59602602
# 2 0.08806948   2.976316 0.04403474  28.77632 0.04403474  -2.023684 0.04403474
# 3 0.08806948   2.976316 0.04403474  28.77632 0.04403474  -2.023684 0.04403474
# 4 2.57356011  -2.049492 2.57020210  22.88756 2.55275509  -9.394162 2.53407818
# 5 0.84897320 -13.171277 0.92719424  12.39681 0.83339460 -19.912411 0.89597516
# 6 1.49422224  -4.418605 1.44835606  19.11628 1.49422224  -9.574419 1.49097565
#     bio9_mean    bio9_sd
# 1  -4.9622449 1.59602602
# 2   4.9763158 0.04403474
# 3   4.9763158 0.04403474
# 4  -0.1494924 2.57020210
# 5 -11.4712766 0.92719424
# 6  -3.1186047 1.44835606
summary(rf_data)
#  occurrence_ID          genus             longitude         latitude    
#  Min.   :1.000e+09   Length:10048       Min.   : 6.558   Min.   :44.02  
#  1st Qu.:9.993e+09   Class :character   1st Qu.: 8.096   1st Qu.:44.78  
#  Median :9.995e+09   Mode  :character   Median : 9.068   Median :45.23  
#  Mean   :9.952e+09                      Mean   : 9.102   Mean   :45.28  
#  3rd Qu.:9.998e+09                      3rd Qu.:10.155   3rd Qu.:45.79  
#  Max.   :1.000e+10                      Max.   :12.346   Max.   :46.62  
#    occurrence           subcID            elev_mean           elev_sd       
#  Min.   :0.000000   Min.   :511474128   Min.   :   0.318   Min.   :  0.000  
#  1st Qu.:0.000000   1st Qu.:511863427   1st Qu.: 142.437   1st Qu.:  1.954  
#  Median :0.000000   Median :512137948   Median : 442.903   Median : 31.874  
#  Mean   :0.004777   Mean   :512146211   Mean   : 781.424   Mean   : 60.759  
#  3rd Qu.:0.000000   3rd Qu.:512421120   3rd Qu.:1252.822   3rd Qu.: 99.626  
#  Max.   :1.000000   Max.   :512897870   Max.   :4458.342   Max.   :591.182  
#    flow_mean            flow_sd          slopgrad_mean      slopgrad_sd      
#  Min.   :    0.014   Min.   :    0.000   Min.   :0.00000   Min.   :0.000000  
#  1st Qu.:    0.039   1st Qu.:    0.053   1st Qu.:0.01205   1st Qu.:0.009875  
#  Median :    0.078   Median :    0.137   Median :0.17128   Median :0.092957  
#  Mean   :   34.769   Mean   :   70.058   Mean   :0.23033   Mean   :0.128213  
#  3rd Qu.:    0.383   3rd Qu.:    0.758   3rd Qu.:0.39447   3rd Qu.:0.204200  
#  Max.   :19185.442   Max.   :27041.544   Max.   :1.46110   Max.   :1.196673  
#    bio10_mean        bio10_sd        bio11_mean          bio11_sd      
#  Min.   :-5.543   Min.   :0.0000   Min.   :-24.6678   Min.   :0.00000  
#  1st Qu.:15.366   1st Qu.:0.0000   1st Qu.: -2.3511   1st Qu.:0.00000  
#  Median :20.965   Median :0.1136   Median :  2.8983   Median :0.07806  
#  Mean   :18.820   Mean   :0.3535   Mean   :  0.1657   Mean   :0.31531  
#  3rd Qu.:23.150   3rd Qu.:0.5644   3rd Qu.:  3.7500   3rd Qu.:0.48368  
#  Max.   :24.750   Max.   :3.8672   Max.   :  7.2955   Max.   :3.96058  
#    bio5_mean        bio5_sd         bio6_mean          bio6_sd       
#  Min.   :-0.45   Min.   :0.0000   Min.   :-29.950   Min.   :0.00000  
#  1st Qu.:20.21   1st Qu.:0.0000   1st Qu.: -7.893   1st Qu.:0.00000  
#  Median :26.10   Median :0.1197   Median : -1.940   Median :0.06959  
#  Mean   :24.10   Mean   :0.3567   Mean   : -4.843   Mean   :0.31084  
#  3rd Qu.:28.75   3rd Qu.:0.5671   3rd Qu.: -0.950   3rd Qu.:0.47998  
#  Max.   :31.25   Max.   :3.8565   Max.   :  5.150   Max.   :3.94239  
#    bio9_mean          bio9_sd        
#  Min.   :-22.351   Min.   : 0.00000  
#  1st Qu.: -1.067   1st Qu.: 0.00000  
#  Median :  3.662   Median : 0.09524  
#  Mean   :  3.197   Mean   : 0.36299  
#  3rd Qu.:  5.850   3rd Qu.: 0.53329  
#  Max.   : 24.150   Max.   :10.55831
```

Convert to factor for classification
```r
rf_data$occurrence <- as.factor(rf_data$occurrence)
```


See number of presences and absences
```r
table(rf_data$occurrence)
#     0     1
# 10000    48
```

### Random Forest using the Down-sampling method (Valavi et al., 2021)

```r
# Define number of presences
pres_num <- nrow(rf_data[rf_data$occurrence == 1,]) 

# Define sample size of each random forest bag
sample_size <- pres_num/nrow(rf_data) 
sample_size        
# 0.00477707 
```

set.seed(37)
Run Random Forest with the Ranger package

```r
# Random Forest with down-sampling using the Ranger package. In case you need the output to be a probability of occurrence, set 'probability=T'
rg_all <- ranger(rf_data$occurrence ~ ., 
                 data = rf_data[,7:ncol(rf_data)],
                 num.trees = 1000,
                 mtry = 6,
                 replace = T,
                 sample.fraction = c(sample_size,sample_size),
                 oob.error = T,
                 keep.inbag = TRUE,
                 importance = 'impurity', 
                 probability = F) 

# Type:                             Classification
# Number of trees:                  1000
# Sample size:                      10048
# Number of independent variables:  16
# Mtry:                             6
# Target node size:                 10
# Variable importance mode:         impurity
# Splitrule:                        gini
# OOB prediction error (Brier s.):  0.1173819
```
mtry: Number of variables to possibly split at in each node.
num.trees: Number of trees.
replace: Sample with replacement.
oob.error: Save how often observations are in-bag in each tree.
keep.inbag: Save how often observations are in-bag in each tree
importance: The ’impurity’ measure is the Gini index for classification
probability: Grow a probability forest as in Malley et al. (2012)

Variable importance plot
```r
v1 <- vip(rg_all,num_features=30, horizontal=T)
v1

```
![picture 1](figures/Var_imp.png)  




###  Predict with fitted model  

Import table with the statistics per subcatchment for all the subcatchments of the basin
```r
subc_data <- fread(paste0(data_dir,"/stats_basin_1173421_for_RF.txt"),  
                        sep= " ", header = T)
str(rf_data)
str(subc_data)

head(subc_data)
```


Predict
```r
pred <- predict(rg_all, data=subc_data, index=2)
head(pred$prediction)
# [1] 0 0 0 1 0 1
# Levels: 0 1

# Or, in case we deal with probabilities:
#              0         1
# [1,] 0.6115968 0.3884032
# [2,] 0.5553770 0.4446230
# [3,] 0.6312937 0.3687063
# [4,] 0.4376984 0.5623016
# [5,] 0.6238079 0.3761921
# [6,] 0.4169905 0.5830095

```

We then need to reclassify the sub-catchment raster based on the predicted occurrences or probabilities of occurrence.
This is much faster in GRASS than in R! To do this, we need to create a file including the reclassification rules.
Each subcatchment ID will be assigned its respective occurrence (0-1) or probability of occurrence.
(e.g., 56203402 = 0.3007245), while no data will be assigned with -9999.


Create file with reclassification rules to reclassify
```r
prediction <- data.frame(subcID = subc_data[,1], 
                        pred_occur=as.numeric(as.character(pred$predictions)))

setDT(prediction)

# In case we deal with 0-1 occurrences: 
prediction$reclass <- str_c(prediction$subcID," = ", 
                            prediction$pred_occur)

prediction <- prediction[,'reclass']

prediction <- rbind.data.frame(as.data.frame(prediction), "* = -9999")

# Export file with reclassification rules
fwrite(prediction, paste0(data_dir,"/", taxon, "_reclass_rules_pred_prob.txt"),
                          sep="\t", row.names=F, quote=F, col.names=F)    
```

Function that calls GRASS to perform the reclassification.
```r
raster_reclass_fun <- function(raster_path, reclass_rules, output) {

  # Call external gdal command of gdallocationinfo()
  processx::run(paste0(script_dir, "/raster_reclass.sh"),
                       args = c(raster_path, reclass_rules, output),
                       echo = F)$stdout

}
```
Call the function
```r
raster_reclass_fun(raster_path = subc_path,
          reclass_rules = paste0(data_dir,"/", taxon, "_reclass_rules_pred_prob.txt"),
          output = paste0(data_dir,"/",taxon,"_pred_prob.tif")

)
```

Here is an example of a probability map. 
![image 9](figures/prediction.png)
![image 10](figures/pred_with_points.png)


# Modelling excercise

Now comes the fun part :-) All participants gets a  genus assigned, and the task is to run the model and upload the results to Nextcloud at https://nimbus.igb-berlin.de/index.php/s/27XEHwirmKB9TMb. We will then stack the individual projections into one map.

## Stacking of individual model projections
```r
# Get all maps in the directory
output_map_names <- list.files(data_out, pattern = "pred_prob.tif", full.names = TRUE)
output_maps <- terra::rast(output_map_names)

# Plot the modelled richness (sum of all maps)
richness <- sum(output_maps)
plot(richness, col=viridis(10))

```



# Bonus material 

## Snapping
Given the potential mismatches between the coordinates of each occurrence and the
modelled stream network, some observation would not overlap.

![Snapping step 3](./figures/snap_05.png)


Function that snaps points to the stream network
```r
snap_to_network <- function(data_dir, data_out, taxon, basinID) {
  
  
  processx::run(paste0(home_dir, "/scripts/functions/snapping.sh"),
                args = c(data_dir, data_out, taxon, basinID),
                echo = T)
}


```

Call the function
```r
points_snap <- snap_to_network(data_dir = data_dir, data_out = data_out,
                               taxon = taxon, basinID = basinID)

```

Export snapped points
```r
fwrite(snapped, paste0(data_out, taxon, "_points_snapped.csv"),
            row.names = F, quote = F, sep = " ")
```



## Stream connectivity

In this section we touch on example connectivity indices of a stream network. We will transform the spatial network into a graph which allows to query different metrics very efficiently. The graph is built by two columns: the "stream" and "next_stream" ID of the .gpkg stream network vector file.


First we set some options and functions:
```r
options(scipen=999) # avoid any exponential numbers in the reclassification

###  Plotting function for a quick visualization
plot_net <- function(network){
  rglplot(network, layout=layout.fruchterman.reingold , vertices=network$stream, vertex.size=2, label.cex=0.1, vertex.label.dist=0.5, vertex.color="orange", edge.arrow.size=0.6) #edge.color = E(g)$strahler)
}
```

Import the stream network as a graph, i.e. a sparse matrix that holds the information about the connectivity. 
```r
# Load the network as a graph. First create a connection to the vector SQLite database
conn <- dbConnect(drv=RSQLite::SQLite(), dbname="order_vect_59_clip.gpkg") # get connection
conn_table <- dbListTables(conn) # ## list all tables
conn_table <- rev(conn_table)[1] # usually the last item --> revert and pick the first

# Read the database
streams_dt <- dbGetQuery(conn=conn, statement=paste0("SELECT * FROM '", conn_table, "'"))
setDT(streams_dt) # convert to data.table
dbDisconnect(conn) # close db connection

# Remove some messy and obsolete columns
streams_dt$cat <-  NULL 
streams_dt$geom <- NULL 
streams_dt$fid <-  NULL
streams_dt$cat_ <-  NULL 
streams_dt$stream_copy <-  streams_dt$stream # copy for a later merge

# Create a graph. The first two columns will build the graph, other columns remain as attributes
g <- graph.data.frame(streams_dt, directed = T, vertices=NULL)
g
```

We will use the large network for calculations, but let's first subset it such that we can visualize it. Pick a random stream reach and get the upstream catchment.

```r
# First identify the stream reaches of the catchment
sub_edges <- subcomponent(g, "512220470", mode = c("in"))

# Subset the large graph
sub_g <- subgraph(g, sub_edges)
E(sub_g) # 47 edges = stream reaches



# Plot the graph, zoom in and check the single elements
plot_net(sub_g)
```


We can obtain some summary statistics of the graph:
```r
# Number of edges and vertices
E(sub_g) # edges
V(sub_g) # vertices

sub_g[] # sparse matrix
summary(sub_g)

# Check the edge (=stream reach) and vertex (=node) attributes
edge_attr_names(sub_g)
edge_attr(sub_g, name="stream_copy", index = E(sub_g))
edge_attr(sub_g, name="strahler", index = E(sub_g)) # stream order

vertex_attr_names(g)
vertex_attr(g)

# Which stream reaches are most connected in the network, which have the highest number of direct connections?
g_degree <- igraph::degree(sub_g)
g_degree

hist(g_degree, breaks=20)
hist(log(g_degree), breaks=50)
```


If traversing through the network - which parts have a high number of possible connections?
We can calculate the **edge betweenness centrality**, defined by the number of shortest paths going through a given stream reach.

```r
eb <- edge_betweenness(g, directed=T) # takes 1 min on entire graph
```

**Question:** Why would this information be useful, in which context?


Add the attributes to the graph
```r
g <- set_edge_attr(g, name="eb", index = E(g), value=eb)
# Check the edge attributes
edge_attr_names(g)
head(edge_attr(g, name="eb", index = E(g)), 50)
```

Next we want to plot the edge betweenness. Thee most efficient way is to simply reclassify the sub-catchment raster layer such that it will hold the new edge betweenness information. 

First write the reclassification rules as a .txt-file to disk.
```r
g_table <- as_long_data_frame(g) 
setDT(g_table)
eb_reclass <- g_table[,c("stream_copy", "eb")]
eb_reclass <- rbind.data.frame(eb_reclass, c("*", "NULL")) # add the wildcard for any non-matching sub-catchments
eb_reclass$GRASS <- paste0(eb_reclass$stream_copy, " = ", eb_reclass$eb) # add the "="
eb_reclass # check data

# Write the "GRASS" column to disk
fwrite(eb_reclass[,"GRASS"], paste0(data_dir, "/reclass_rules_edge_betweenness.txt"),
       sep="\t", row.names=F, quote=F, col.names=F)
```

Next we use the same reclassification function as done above, using GRASS GIS. 
```r
raster_reclass_fun(raster_path = subc_path,
                   reclass_rules = paste0(data_dir, "/reclass_rules_edge_betweenness.txt"),
                   output = paste0(data_dir,"/edge_betweenness.tif")
                   )
```

Finally plot the edge betweenness layer. The values correspond to the number of possible connections.
```r
eb_rast <- raster(paste0(data_dir, "/edge_betweenness.tif"))
plot(eb_rast, col=viridis(10))
plot(eb_rast, col=viridis(10), zlim=c(0, 10000000)) # test different values
plot(eb_rast, col=viridis(10), zlim=c(0, 1000000)) 
```
This plot shows the edge betweenness for the entire basin. Lighter colors refer to stream reaches (or sub-subcatchments) that have a high edge betweenness.
![picture 11](./figures/edge_betweenness.png)


# Close-up and questions
