#!/bin/bash

###############################################################

export DATA=$1
export DATASET=$2
export ID=$4  # basin ID
export FINAL=$5

export TMPTAX=$DATA/tmp
[ ! -d $TMPTAX ] && mkdir $TMPTAX



# Name of column for unique ID
export SITE=$( awk 'NR==1 {print $1}' $DATASET )


grass78  -f -text --tmp-location  -c $DATA/basin_${ID}.tif  <<'EOF'


# Points in subcatchment
v.in.ascii  input=$DATASET output=taxon \
 separator=space  x=3 y=4 skip=1 cat=1 columns="occurrence_ID integer, genus varchar, longitude double precision, latitude double precision, \
  subcID integer, occurrence integer" --overwrite 


#  Read files with subcatchments
r.in.gdal --o input=$DATA/subcatchment_${ID}.tif \
    output=subc 

##  Procedure to create a subcatchment raster but only with those where points overlap
subc=$(v.db.select taxon | awk -F'|' 'NR > 1 {print $6}' ORS=' ' | xargs -n1 | sort | uniq)


[  -f $TMPTAX/reclass_${ID}.txt ] && rm $TMPTAX/reclass_${ID}.txt

for i in $subc; do
  echo $i = 1 >> $TMPTAX/reclass_${ID}.txt
done
echo '* = NULL' >> $TMPTAX/reclass_${ID}.txt

r.reclass --o input=subc output=subc_recl rules=$TMPTAX/reclass_${ID}.txt
r.mapcalc --o "subconly = if(subc_recl==1,subc,null())"

for NUM in 5 6 9 10 11 ; do 
  export file=$DATA/CHELSA_bio${NUM}_1981-2010_V.2.1.tif
#  for file in $(find $DATA -name 'CHELSA_bio*.tif');
# do
  export VAR=$(basename $file | awk -F_ '{print $2}')
  r.external input=$file output=$VAR --overwrite
  
  echo "subcID" ${VAR}_min ${VAR}_max ${VAR}_range ${VAR}_mean ${VAR}_sd > $FINAL/stats_${VAR}.txt  

 r.univar -t --o map=$VAR zones=subconly separator=comma |  \
  awk -F, 'NR > 1 {print $1, $5, $6, $7, $8, $10}' \
      >> $FINAL/stats_${VAR}.txt

done

EOF
exit




