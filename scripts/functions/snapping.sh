

#!/bin/sh

##### needs parallelization!

export DIR=$1
export OUT=$2
export TAXON=$3
export ID=$4 # Basin ID
export TMPDIR=$2/tmp
export DATASET=$OUT/${TAXON}_pres_subcIDs.csv
echo $TMPDIR 
echo 'running'

echo $basinID
[ ! -d $TMPDIR ] && mkdir $TMPDIR
[ ! -d $TMPDIR/snappoints ] && mkdir $TMPDIR/snappoints 
export OUTDIR=$TMPDIR/snappoints

# Macro-basin
# Name of column for unique occurrence ID
export SITE=$( awk 'NR==1 {print $1}' $DATASET )




###### here SELECT is the layer's name, but probably it should not be 

##################################
######     START GRASS
grass78 -f -text --tmp-location -c $DIR/basin_${ID}.tif  <<'EOF'


# Points in microbasin 
v.in.ascii  input=$DATASET output=taxon \
 separator=space  x=3 y=4 skip=1 cat=1 columns="occurrence_ID integer, taxon varchar, longitude double precision, latitude double precision, \
  occurrence integer, subcID integer" --overwrite 

# v.in.ogr -o input=$DIR/${TAXON}_BasisDataBasinsIDs.gpkg layer=orig_points output=taxon \
# type=point  where="CompUnitID = ${ID}"  key=$SITE   

# Raster with microbasins
#r.in.gdal input=$TMPDIR/basins/basin_${ID}/bid_${ID}_micb.tif output=micb
r.in.gdal input=$DIR/subcatchment_${ID}.tif  output=micb

# list of Site_ID of points available in microbasin
RANGE=$(v.db.select -c taxon col=$SITE)

# for loop to make the snap of each point at a time
for PN in $RANGE; do

    # select micro basin id at point
    MB=$(v.db.select taxon | awk -F'|' -v X="${PN}" '$1==X {print $6}')

    # vector line (stream reach) associated with point

    # v.in.ogr  --o input=$OUTDIR/order_vect.gpkg \
    # layer=SELECT output=streams_${PN}_${MB} type=line key=stream \
    # where="stream = ${MB}"

    v.in.ogr  --o input=$DIR/order_vect.gpkg \
    layer=SELECT output=streams_${PN}_${MB} type=line key=stream \
    where="stream = ${MB}"


    # extract point
    v.extract --o input=taxon type=point cats=${PN} output=point_${PN}

    # extract microbasin of stream reach $PN as raster
    r.mapcalc --o "micr_${PN} = if(micb != ${MB}, null(), 1)"        
          
    # make the raster a vector points
    r.to.vect --o input=micr_${PN} output=micr_vp_${PN} type=point     
    
    # of how many pixels the raster consist? 
    # 1 if stream reach with only one pixel
    # meaning the points already overlap
    NUMP=$(v.info micr_vp_${PN}  | awk '/points/{print $5}')
            
    if [ $NUMP -eq 1 ]
    then
        v.net --o -s input=streams_${PN}_${MB}  points=point_${PN} \
        output=snap_${PN} operation=connect threshold=90 arc_layer=1 \
        node_layer=2

        v.out.ascii input=snap_${PN} layer=2 separator=comma \
        > ${OUTDIR}/coords_${PN}
    
    else 

        v.distance -pa from=micr_vp_${PN} to=micr_vp_${PN}  upload=dist \
          > $TMPDIR/dist_mat_p${PN}_${ID}_${MB}.txt
          
        # calculate maximum distance between all points in microbasin
        MAXDIST=0
        for i in \
        $( seq -s' ' 2 $(v.info micr_vp_${PN}  | awk '/points/{print $5}') )
        do
          newmax=$(awk -F'|' -v X="$i" '{print $X}' \
          $TMPDIR/dist_mat_p${PN}_${ID}_${MB}.txt | sort -n | tail -n1)
          if (( $(echo "$newmax > $MAXDIST" | bc -l) ));then MAXDIST=$newmax;fi
        done

        v.net --o -s input=streams_${PN}_${MB}  points=point_${PN} \
        output=snap_${PN} operation=connect threshold=$MAXDIST arc_layer=1 \
        node_layer=2

        v.out.ascii input=snap_${PN} layer=2 separator=comma \
        > ${OUTDIR}/coords_${PN}

        rm $TMPDIR/dist_mat_p${PN}_${ID}_${MB}.txt
    fi 
done


#  Join all single tables in one file
echo XcoordSnap,YcoordSnap,Site_ID_snap > ${OUTDIR}/coords_all_${ID}.csv
for i in $RANGE; do cat ${OUTDIR}/coords_${i} \
>> ${OUTDIR}/coords_all_${ID}.csv; done

#  Read back to GRASS as a vector point
v.in.ascii --o input=${OUTDIR}/coords_all_${ID}.csv \
output=allPoints_${ID} \
columns='XcoordSnap double precision, YcoordSnap double precision, Site_ID_snap int' \
skip=1 x=1 y=2 z=0 separator=comma

# join attribute table from original file
v.db.join map=allPoints_${ID} column=Site_ID_snap other_table=taxon \
other_column=$SITE

# create the new vector gpkg file
v.out.ogr  -s --overwrite input=allPoints_${ID} type=point  \
output=${OUTDIR}/basin_${ID}.gpkg format=GPKG output_layer=snapped

EOF


### Merge the snapped files
# First create the gpkg file
export FILE=$TMPDIR/snappoints/basin_${ID}.gpkg

ogrmerge.py -o $OUT/${TAXON}_points_snapped.gpkg  $FILE -f "GPKG" -single -nln LocationSnap -overwrite_ds

# Make also the csv file
ogr2ogr -f "CSV" -lco STRING_QUOTING=IF_NEEDED $OUT/tmp/snappoints/${TAXON}_points_snapped.csv $OUT/${TAXON}_points_snapped.gpkg

## Order columns of the csv
awk -F',' '{print  $4,$5,$6,$7,$1,$2,$8,$9}'  $TMPDIR/snappoints/${TAXON}_points_snapped.csv \
  > $OUT/${TAXON}_points_snapped_tmp.csv && mv $OUT/${TAXON}_points_snapped_tmp.csv $OUT/${TAXON}_points_snapped.csv


# remove temporal files and folders
rm -rf $TMPDIR/snappoints





exit
