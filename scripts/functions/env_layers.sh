#!/bin/bash



###############################################################

export DATA=$1
export DATASET=$2
export ID=$3  # basin ID
export FINAL=$4

export TMPTAX=$DATA/tmp
[ ! -d $TMPTAX ] && mkdir $TMPTAX


export BASIN=$TMPTAX/basins
[ ! -d $TMPTAX/basins ] && mkdir $TMPTAX/basins

[ ! -d $TMPTAX/r_univar ] && mkdir $TMPTAX/r_univar
export OUTDIR=$TMPTAX/r_univar


# Name of column for unique ID
export SITE=$( awk 'NR==1 {print $1}' $DATASET )


####     slope, accumulation, order, elevation, upstream distance


##################################
######     START GRASS
grass78  -f -text --tmp-location  -c $DATA/basin_${ID}.tif  <<'EOF'

## Points in microbasin
v.in.ascii  input=$DATASET output=taxon \
 separator=space  x=3 y=4 skip=1 cat=1 columns="occurrence_ID integer, genus varchar, longitude double precision, latitude double precision, \
  occurrence integer, subcID integer" --overwrite 

## Read in subcatchment raster
r.in.gdal --o input=$DATA/subcatchment_${ID}.tif output=subc 

##  Procedure to create a subcatchment raster but only with those where points overlap
subc=$(v.db.select taxon | awk -F'|' 'NR > 1 {print $6}' ORS=' ' | xargs -n1 | sort | uniq)

## remove file with reclassification rules if it already exists
[  -f $TMPTAX/reclass_${ID}.txt ] && rm $TMPTAX/reclass_${ID}.txt

for i in $subc; do
  echo $i = 1 >> $TMPTAX/reclass_${ID}.txt
done
echo '* = NULL' >> $TMPTAX/reclass_${ID}.txt

r.reclass --o input=subc output=subc_recl rules=$TMPTAX/reclass_${ID}.txt
r.mapcalc --o "subconly = if(subc_recl==1,subc,null())"

##  Calculate the statistics for each microbasin for each variable and save in csv file
for VAR in flow  elev slopgrad ; do  # also :  slopcmax slopcmin slopdiff

echo 'calculating' $VAR
   VARNAME=$(basename $VAR .tif)
  r.external  input=$DATA/${VARNAME}_${ID}.tif  output=$VARNAME --overwrite
  # v.what.rast -p map=taxon raster=$VARNAME > $OUTDIR/stats_atpoint_${VARNAME}.csv   ## we don't need the values at point currently
  r.univar -t --o map=$VARNAME zones=subconly  separator=comma | \
      awk -F, 'BEGIN{OFS=",";} {print $1, $5, $6, $7, $8,  $10 }' \
      >$OUTDIR/stats_${VARNAME}.csv
done



rm $TMPTAX/*${ID}*

EOF





#### Join environmental layers

[ ! -d $DATA/statsOUT ] && mkdir $DATA/statsOUT
OUTSTAT=$DATA/statsOUT

###   Part I
###  Join tables from sc08 (note that the rows or records in these tables represent each microbasin)

for VAR in slopgrad elev flow  ; do  #
    echo "---- Joining ${VAR} ----"
    if [ -f $OUTSTAT/stats_${VAR}.csv ];then rm $OUTSTAT/stats_${VAR}.csv; fi
    echo "zone,${VAR}_min,${VAR}_max,${VAR}_range,${VAR}_mean,${VAR}_stddev" > $OUTSTAT/stats_${VAR}.csv 
    FILES=$( find $TMPTAX/r_univar/* -name "*${VAR}.csv" ! -name '*atpoint*')
    cat $FILES > $TMPTAX/tmp_${VAR}.csv
    awk -F, '!(/zone/)' $TMPTAX/tmp_${VAR}.csv > $TMPTAX/tmp_${VAR}2.csv
    cat $TMPTAX/tmp_${VAR}2.csv >> $OUTSTAT/stats_${VAR}.csv
    rm $TMPTAX/tmp_${VAR}.csv $TMPTAX/tmp_${VAR}2.csv
done

# echo "stream,strahler,out_dist,elev_drop" > $OUTSTAT/stats_order.csv
# FILES=$( find $TMPTAX/r_univar/* -name '*order*' ! -name '*atpoint*')
# cat $FILES > $TMPTAX/TMPGEN_order.csv
# awk -F, '!(/stream/)' $TMPTAX/TMPGEN_order.csv > $TMPTAX/TMPGEN_order2.csv
# cat $TMPTAX/TMPGEN_order2.csv >> $OUTSTAT/stats_order.csv
# rm $TMPTAX/TMPGEN_order2.csv $TMPTAX/TMPGEN_order.csv


######################################################################

### Part II
### Assign to the original table, that is, to each Site_ID, the environmental values given in the previous joined tables by using the microbasin as the joining ID

cat $DATASET | awk 'BEGIN{OFS=",";} {print $1, $6}' > $TMPTAX/tojoin_ENV.csv

for VAR in   flow slopgrad elev ; # also :  slopcmax slopcmin slopdiff
do
      if [ "$VAR" == "order" ]
      then
        echo "stream,strahler,out_dist,elev_drop"  > $TMPTAX/tmp_${VAR}.csv
      else
        echo "zone,${VAR}_min,${VAR}_max,${VAR}_range,${VAR}_mean,${VAR}_stddev" > $TMPTAX/tmp_${VAR}.csv
      fi
          for i in $( seq 1 $( awk 'FNR > 1' $DATASET | wc -l  ) )
          do
            subcID=$(cat $TMPTAX/tojoin_ENV.csv | awk -F, -v ROW=$i 'NR==1+ROW {print $2}')
            echo "calculating $VAR subcatchment $i"
            line=$(cat $OUTSTAT/stats_${VAR}.csv | awk -F, -v subcID=$subcID '$1 == subcID')
                if [ -z "$line" ]
                then
                    echo "$subcID,Empty" >> $TMPTAX/tmp_${VAR}.csv
                else
                    echo $line >> $TMPTAX/tmp_${VAR}.csv
                fi
          done
      paste -d "," $TMPTAX/tojoin_ENV.csv $TMPTAX/tmp_${VAR}.csv > $FINAL/stats_${VAR}_complete.csv
done

#rm $TMPTAX/tojoin_ENV.csv $TMPTAX/tmp_*.csv
#rm -rf $TMPTAX/r_univar

# cat $(find $TMPTAX/r_univar/* -name 'stats_atpoint_elev.csv') | sort -n  > $TMPTAX/elev_atpoint.txt
# cat $(find $TMPTAX/r_univar/* -name 'stats_atpoint_flow.csv') | sort -n  > $TMPTAX/flow_atpoint.txt
# cat $(find $TMPTAX/r_univar/* -name 'stats_atpoint_flow.csv') | sort -n  > $TMPTAX/flow_atpoint.txt
# cat $(find $TMPTAX/r_univar/* -name 'stats_atpoint_slopcmin.csv') | sort -n  > $TMPTAX/slopcmin_atpoint.txt
# cat $(find $TMPTAX/r_univar/* -name 'stats_atpoint_slopcmax.csv') | sort -n  > $TMPTAX/slopcmax_atpoint.txt
# cat $(find $TMPTAX/r_univar/* -name 'stats_atpoint_slopdiff.csv') | sort -n  > $TMPTAX/slopdiff_atpoint.txt
# cat $(find $TMPTAX/r_univar/* -name 'stats_atpoint_slopgrad.csv') | sort -n  > $TMPTAX/slopgrad_atpoint.txt

### Values at point are not needed because we will work with subcatchments

# echo "Site_ID,Elevation,FlowAccum,flow,SlopcMin,SlopcMax,SlopDiff,SlopGrad" > $FINAL/elev_flow_slope_atpoint.csv

# paste -d','   \
#     <(cut -d'|' -f1 $TMPTAX/elev_atpoint.txt) \
#     <(cut -d'|' -f2 $TMPTAX/elev_atpoint.txt) \
#     <(cut -d'|' -f2 $TMPTAX/flow_atpoint.txt) \
#     <(cut -d'|' -f2 $TMPTAX/flow_atpoint.txt) \
#     <(cut -d'|' -f2 $TMPTAX/slopcmin_atpoint.txt) \
#     <(cut -d'|' -f2 $TMPTAX/slopcmax_atpoint.txt) \
#     <(cut -d'|' -f2 $TMPTAX/slopdiff_atpoint.txt) \
#     <(cut -d'|' -f2 $TMPTAX/slopgrad_atpoint.txt) \
#     >> $FINAL/elev_flow_slope_atpoint.csv

# rm $TMPTAX/{elev,flow*,slop*}_atpoint.txt

exit







