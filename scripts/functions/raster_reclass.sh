

#!/bin/sh


export MBFILE=$1
export RULES=$2
export OUTPUT=$3

grass78 -f  --tmp-location  -c $MBFILE   <<'EOF'

    export filename=$(basename $MBFILE .tif  )
    r.in.gdal --o input=$MBFILE  output=subc    --overwrite

    r.reclass input=subc output=pred rules=$RULES --overwrite

    # export map
    r.out.gdal --o -f -m -c createopt="COMPRESS=DEFLATE,ZLEVEL=9" type=Int32  format=GTiff nodata=-9999 input=pred output=$OUTPUT

# done

EOF
